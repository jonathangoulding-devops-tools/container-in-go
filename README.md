# Container in Go

Simple exploration into building containers with Go. 

### Sample of end result
![alt text](/img/demo.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- [Go](https://golang.org/doc/install)
- Linux file system

### Installing

Getting a linux file system. 
```bash
wget http://cdimage.ubuntu.com/ubuntu-base/releases/16.04/release/ubuntu-base-16.04.4-base-amd64.tar.gz
```

Then 
```
extract and move the downlaoded folder to /home 
rename the folder to ubuntufs
```
Or 
Change the file path in the code
      
```go
must(syscall.Chroot("/PATH_TO_FOLDER/ubuntufs"))
```

### Runnning a Container

Simple bash command 
```bash
go run main.go run echo container camp
```

Or

Create container with own file system and hostanme
```bash
sudo go run main.go run /bin/bash
```

## Running the tests

To see if you are inside the running container: 

Check your hostname:
```bash
hostname
```
Then start the container:
```bash
sudo go run main.go run /bin/bash
```

Then check the hostname is diffrent: 
```
hostname
```

The container hostname is set to `containerHost` so you should see this as the hostname/

Similar tests can be done with `ps` to demostarte the seperation of running processes

- WIP - No automated tests

## Built With

* [Go](https://golang.org/)

## Authors

* **Jonathan Goulding**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

- https://www.youtube.com/watch?v=Utf-A4rODH8
- https://github.com/lizrice
- https://kevincrawley.io/posts/got-namespaces-pt1/
